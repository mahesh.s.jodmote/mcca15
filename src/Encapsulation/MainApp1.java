package Encapsulation;

public class MainApp1 {
    public static void main(String[] args) {
        Employee e1=new Employee();

        //read private data through getter
        int id= e1.getEmpId();
        double salary=e1.getEmpsalary();
        System.out.println("ID is:"+id);
        System.out.println("Salary is:"+salary);

        System.out.println("=======================");

        //modify private data through setter
        e1.setEmpId(201);
        e1.setEmpsalary(35000.25);
        System.out.println("emp id:"+e1.getEmpId());
        System.out.println("emp salary is:"+e1.getEmpsalary());

    }
}
