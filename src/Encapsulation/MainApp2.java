package Encapsulation;

public class MainApp2 {
    public static void main(String[] args) {
        Mobile m1=new Mobile();
        System.out.println("Company name is "+m1.company);

        //encapsulation
        Mobile.Ram r1=m1.new Ram();
        r1.displayInfo();

        //encapsulation
        Mobile.Processor p1=m1.new Processor();
        p1.displayName();
    }
}
