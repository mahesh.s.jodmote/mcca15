package Encapsulation;
//outer class
public class Mobile {
    String company="Samsung";

    //inner class
    class Ram{
        void displayInfo(){
            System.out.println("Ram size is 8 Gb");
        }
    }

    //inner class
    class Processor{
        void displayName(){
            System.out.println("Processor is Snapdragon");
        }
    }
}
