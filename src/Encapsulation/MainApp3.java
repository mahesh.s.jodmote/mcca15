package Encapsulation;

public class MainApp3 {
    public static void main(String[] args) {
        //run time implementation
        Shape s1=new Shape() {
            //Anonymous class
            @Override
            public void area() {
                double result=3.14*5*5;
                System.out.println("Area of circle is:"+result);
            }
        };
        s1.area();

        Shape s2=new Shape() {
            //Anonymous class
            @Override
            public void area() {
                double result=0.5*4*5;
                System.out.println("Area of triangle:"+result);
            }
        };
        s2.area();
    }
}
