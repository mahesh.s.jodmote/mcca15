package Encapsulation;

public class Employee {

    private int empId=101;
    private double empsalary=1200;

    //read access
    public int getEmpId(){
        return empId;
    }

    //write access
    public void setEmpId(int empId){
        this.empId=empId;
    }

    //read access
    public double getEmpsalary(){
        return empsalary;
    }

    //write access
    public void setEmpsalary(double empsalary){
        if (empsalary>0){
            this.empsalary=empsalary;
        }
    }
}
