//accept start & end point from user print the square of all the even number in that given range
package loopingStatement;
import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import java.util.Scanner;

public class forloopDemo3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Start point");
        int start=sc.nextInt();
        System.out.println("Enter second point");
        int end=sc.nextInt();
        for (int a=start;a<=end;a++){
            if(a%2==0){
                System.out.println(a*a);
            }
        }
    }
}
