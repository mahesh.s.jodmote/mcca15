package loopingStatement;

import java.util.Scanner;

public class whileDemo1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int no=0;
        while(no>=0){
             System.out.println("Enter number");
            no=sc.nextInt();
            System.out.println("Entered number is:"+no);
        }
        System.out.println("Entered -ve value");
    }
}
