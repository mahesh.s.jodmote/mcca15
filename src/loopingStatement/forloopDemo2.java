package loopingStatement;

import java.util.Scanner;

public class forloopDemo2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter start point");
        int start= sc.nextInt();
        System.out.println("enter end point");
        int end=sc.nextInt();
        for(int a=start;a<=end;a++){
            if(a%2!=0){
                System.out.println(a);
            }
        }
    }
}
