package loopingStatement;

import java.util.Scanner;

//wap to accept start and end point from user print all the odd number in reverse direction
public class forloopDemo4 {
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter first number");
        int start=sc.nextInt();
        System.out.println("Enter end point");
        int end=sc.nextInt();
        for(int a=end;a>=start;a--){
            if(a%2!=0){
                System.out.println(a);
            }
        }
    }
}
