package loopingStatement;

import java.util.Scanner;

public class whileDemo2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int no=0;
        int sum=0;
        while(sum<=50){
            System.out.println("enter no");
            no= sc.nextInt();
            sum+=no;
        }
        System.out.println("sum is:"+sum);
    }
}
