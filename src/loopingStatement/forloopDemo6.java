package loopingStatement;

import java.util.Scanner;

public class forloopDemo6 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter total no of user:");
        double count=sc.nextDouble();
        for(int a=0;a<count;a++){
            System.out.println("Enter user name:");
            String name=sc.next();
            System.out.println("Welcome to the company:-"+name);
        }
    }
}
