package loopingStatement;

import java.util.Scanner;

public class whileDemo3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("1:Addition\n2:/Multiplication\n3:exit");
            int choice= sc.nextInt();
            System.out.println("enter no 1");
            int no1= sc.nextInt();
            System.out.println("enter no 2");
            int no2= sc.nextInt();
            if(choice==1){
                System.out.println(no1+no2);
            }
            else if(choice==2){
                System.out.println(no1*no2);
            }
            else {
                status=false;
            }
        }
    }
}
