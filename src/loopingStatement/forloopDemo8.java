package loopingStatement;

public class forloopDemo8 {
    public static void main(String[]args)
    {
        int i,j,no=1;											//1 2 3
        int col=2;									  //output // 4 5 6
        int row=2;											   // 7 8 9
        for(i=0;i<=row;i++)
        {
            for(j=0;j<=col;j++)
            {
                System.out.print(no+"\t");
                no++;
            }

            System.out.println();
        }
    }
}
