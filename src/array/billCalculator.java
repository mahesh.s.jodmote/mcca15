package array;

public class billCalculator {
    public void calculateBill(double[]amounts){
        double []gstvalue=gstcalculation(amounts);

        //create new array to calculate total amounts
        double[]totalAmounts=new double[amounts.length];

        //perform sum of amounts and gst values
        for(int a=0;a< amounts.length;a++){
            totalAmounts[a]=amounts[a]+gstvalue[a];
        }
        double totalBillAmt=0.0;
        double totalGstAmt=0.0;
        double totalFinalAmt=0.0;

        //sum of all array elements
        for (int a=0;a<amounts.length;a++){
            totalBillAmt +=amounts[a];
            totalGstAmt +=gstvalue[a];
            totalFinalAmt +=totalAmounts[a];
        }
        //present final output to user
        System.out.println("BillAmt\tGstAmt\tTotalAmt");
        System.out.println("=====================================");
        for(int a=0;a<amounts.length;a++){
            System.out.println(amounts[a]+  "\t"+gstvalue[a]+ "\t"+totalAmounts[a]);
        }
        System.out.println("======================================");
        System.out.println(totalBillAmt+"\t"+totalGstAmt+"\t"+totalFinalAmt);

    }
    public double[]gstcalculation(double[]amounts){
        //create new array to store gst values
        double[]gstAmounts=new double[amounts.length];
        for(int a=0;a< amounts.length;a++){
            if(amounts[a]<500){
                gstAmounts[a]=amounts[a]*0.05;
            }
            else {
                gstAmounts[a]=amounts[a]*0.1;
            }
        }
        //return gst array to calculate
        return gstAmounts;
    }
}
