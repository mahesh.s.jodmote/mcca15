package array;

public class arrayDemo1 {
    public static void main(String[] args) {
        //declaration
        int[] data;
        //size allocation
        data = new int[5];
        //initialization
        data[0] = 100;
        data[1] = 200;
        data[2] = 300;
        data[3] = 400;
        data[4] = 500;
        //data[5]=600;
       // System.out.println(data[0]);
        for (int a = 0; a <=4; a++) {
            System.out.println(data[a]);
        }
    }
}
