package array;

import java.util.Scanner;
//bill calculator
public class arrayDemo11 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        billCalculator b1=new billCalculator();
        System.out.println("enter total no.of bills");
        int count= sc.nextInt();
        System.out.println("Enter bill Amount");
        double []amounts=new double[count];
        //accepting bill from user
        for(int a=0;a<count;a++){
            amounts[a]= sc.nextDouble();
        }
        //call method from business class
        b1.calculateBill(amounts);
    }
}
