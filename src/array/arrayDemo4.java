package array;

import java.util.Scanner;

public class arrayDemo4 {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        System.out.println("enter total no of courses");
        int size= sc.nextInt();

        double sum=0.0;
       System.out.println("enter course name:");
        String[]course=new String[size];
        for(int a=0;a<size;a++){
            course[a] = sc.next();
        }
        System.out.println("enter marks:");
        double[] marks=new double[size];
        for(int a=0;a<size;a++){
           marks[a]=sc.nextDouble();
           sum+=marks[a];
        }
        double percent=sum/size;
        System.out.println("sum is:"+sum);
        System.out.println("percent:"+percent);
    }
}
