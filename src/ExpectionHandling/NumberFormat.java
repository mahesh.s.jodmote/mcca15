package ExpectionHandling;

public class NumberFormat {
    public static void main(String[] args) {
        System.out.println("Program started");
        String s1="123abc";
        try{
            int no=Integer.parseInt(s1);
            System.out.println("no is"+no);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
        System.out.println("Program ended");
    }
}
