package ExpectionHandling;

public class NullPointer {
    public static void main(String[] args) {
        System.out.println("Program started");
        String str=null;
        try {
            System.out.println(str.toLowerCase());
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("Program ended");
    }
}
