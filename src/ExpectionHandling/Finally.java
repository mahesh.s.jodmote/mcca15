package ExpectionHandling;

public class Finally {
    public static void main(String[] args) {
        System.out.println("program started");
        String str="Java";
        try {
            System.out.println("try started");
            System.out.println(str.length());
            System.out.println("Try ended");
            System.exit(0);
        } catch (NullPointerException e) {
            System.out.println(e);
        }
        finally {
            System.out.println("closing costly resources");
        }
        System.out.println("Program ended");
    }
}
