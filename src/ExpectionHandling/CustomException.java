package ExpectionHandling;

import java.util.Scanner;

public class CustomException {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter mail");
        String email=sc.next();
        Validation(email);
    }

     static void Validation(String email) {
        if (email.contains("@")&&email.contains(".")){
            System.out.println("Validation Successfully");
        }
        else {
            throw new RuntimeException();
        }
    }
}
