package Overriding;

import java.util.Scanner;

public class MainApp3 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Which Brand you want");
        System.out.println("1.Bisleri\n2.Aqua");
        int choice= sc.nextInt();

        System.out.println("Enter qty");
        int qty= sc.nextInt();
        System.out.println("Enter Price");
        double price= sc.nextDouble();

        if (choice==1){
            Bisleri b1=new Bisleri();
            b1.sellProduct(qty,price);
        } else if (choice==2) {
            Aqua a1=new Aqua();
            a1.sellProduct(qty,price);
        }
        else {
            System.out.println("Invalid Choice..");
            System.out.println("Please choose correct one");
        }
    }
}
