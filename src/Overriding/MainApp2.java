package Overriding;

import java.util.Scanner;

public class MainApp2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Price");
        double price=sc.nextDouble();
        System.out.println("Enter Qty");
        double qty= sc.nextDouble();
        System.out.println("Select Ecommerce platform");
        System.out.println("1.Flipcart\n2.Amazon");
        int choice=sc.nextInt();
        if (choice==1){
            Flipcart f1=new Flipcart();
            f1.sellProduct(price,qty);
        }
        else if (choice==2){
            Amazon a1=new Amazon();
            a1.sellProduct(price,qty);
        }
        else {
            System.out.println("Invalid Choice");
        }

    }
}
