package Overriding;

public class Aqua extends Water{
    @Override
    void sellProduct(int qty, double price) {
       double total=qty*price;
       double finalAmt=total-total*0.1;
        System.out.println("Final Amount:"+finalAmt);
    }
}
