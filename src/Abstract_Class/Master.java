package Abstract_Class;
//wap to implement abstract class with single level inheritance

//super class
public abstract class Master {
    void  test(){
        System.out.println("Test Method");
    }
    abstract void display();

}
