package Abstract_Class;

public abstract class Demo {
    //static and non-static variables
    static int k=20;
    double d=35.25;

    //non-static abstract & concrete method
    abstract void test();
    void display(){
        System.out.println("Display method");
    }

    //static concrete method
    static void Info(){
        System.out.println("Info method");
    }

    //constructor
    Demo(){
        System.out.println("Constructor");
    }

    //static & non-static block
    static {
        System.out.println("static block");
    }
    {
        System.out.println("non-static block");
    }
}
