package Abstract_Class;
//wap to implement abstract class with hierarchical inheritance

//superclass
public abstract class Employee {
    abstract void getDesignation();
    abstract void getSalary();
}
