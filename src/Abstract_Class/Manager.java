package Abstract_Class;
//subclass
public class Manager extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is Manager");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 120000");
    }
}
