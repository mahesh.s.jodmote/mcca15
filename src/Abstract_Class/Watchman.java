package Abstract_Class;
//subclass
public class Watchman extends Employee{
    @Override
    void getDesignation() {
        System.out.println("Designation is watchman");
    }

    @Override
    void getSalary() {
        System.out.println("Salary is 150000");
    }
}
