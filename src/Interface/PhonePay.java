package Interface;

public class PhonePay implements UPI,Wallet{
    @Override
    public void transferAmt(double amt) {
        System.out.println("Amount Transfer:"+amt);
    }

    @Override
    public void makeBillPayment(double amt) {
        System.out.println("Make Bill Payment:"+amt);
    }
}
