package Interface;

public class Visa implements CreditCard{
    @Override
    public void getType() {
        System.out.println("Credit card type is vis");
    }

    @Override
    public void withDraw(double amt) {
        System.out.println("Transaction successful:"+amt);
    }
}
