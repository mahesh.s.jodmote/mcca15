package Interface;

public class MainApp2 {
    public static void main(String[] args) {
        CreditCard credit;
        credit=new Visa();//upcasting
        credit.getType();
        credit.withDraw(25000);
        System.out.println("================================");
        credit=new MasterCard();//upcasting
        credit.getType();
        credit.withDraw(12000);
    }
}
