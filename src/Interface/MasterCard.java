package Interface;

public class MasterCard implements CreditCard{
    @Override
    public void getType() {
        System.out.println("Credit card type is MasterCard");
    }

    @Override
    public void withDraw(double amt) {
        System.out.println("Transaction successful:"+amt);
    }
}
