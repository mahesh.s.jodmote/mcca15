package Overloading;

public class MainApp4 {
    //standard main method
    public static void main(String[] args)
    {
        System.out.println("Main Method 1");
        main(25);
        main('j');
    }

    public static void main(int a)
    {
        System.out.println("A:"+a);
        System.out.println("Main Method 2");
    }

    public static void main(char c) {
        System.out.println("C:"+c);
        System.out.println("Main Method 3");
    }
}
