package Overloading;

import java.util.Scanner;

public class MainApp1 {
    public static void main(String[] args) {
        Student s1=new Student();
        Scanner sc=new Scanner(System.in);
        System.out.println("Select Search Criteria");
        System.out.println("1:search by name");
        System.out.println("2:search by contactno");
        int choice= sc.nextInt();
        if (choice==1){
            System.out.println("enter name");
            String name= sc.next();
            s1.search(name);
        } else if (choice==2) {
            System.out.println("Enter contactNo");
            int contact= sc.nextInt();
            s1.search(s1.contact_no);
        }
        else {
            System.out.println("invalid choice");
        }
    }
}
