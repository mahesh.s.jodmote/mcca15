package AngularMind;

public class Pattern5 {
    public static void main(String[] args) {//* * * * * *
        int a=5;                            //* 1 2 3 4 *
        int b=5;                            //* 1 2 3 4 *
        for (int i=0;i<a;i++)               //* 1 2 3 4 *
        {                                   //* * * * * *
            for (int j=0;j<=b;j++)
            {
                if(i==0||i==4||j==0||j==5)
                {
                    System.out.print("*"+" ");
                }
                else
                {
                    System.out.print(j+" ");
                }
            }
            System.out.println();
        }
    }
}
