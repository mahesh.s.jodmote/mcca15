package AngularMind;

public class Pattern2 {
    public static void main(String[] args) {
        int a=5;                                       //10101
        for (int i=1;i<=a;i++){                        //01010
            for (int j=0;j<a;j++){                     //10101
                System.out.print(((i+j)%2)+" ");       //01010
            }                                          //10101
            System.out.println( );
        }
    }
}
