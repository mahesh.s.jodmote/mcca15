package AngularMind;

public class Pattern4 {
    public static void main(String[] args) {
        int a=5;                        //5 4 3 2 1
        int b=4;                        //5 4 3 2
        for (int row=0;row<=a;row++)    //5 4 3
        {                               //5 4
            int c=5;                    //5
            for (int col=0;col<=b;col++)
            {
                System.out.print(c--+" ");
            }
            System.out.println();
            b--;
        }
    }
}
