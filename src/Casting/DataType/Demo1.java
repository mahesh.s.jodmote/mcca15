package Casting.DataType;

public class Demo1 {
    public static void main(String[] args) {
        int a=15;
        double b=25.23;
        System.out.println(a+"\t\t"+b);

        int c= (int) 35.26; //narrowing
        double d=20;        //widening
    }
}

