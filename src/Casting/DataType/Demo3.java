package Casting.DataType;

public class Demo3 {
    public static void main(String[] args) {
        char ch1='J';
        char ch2='A';

        //widening
        int x1=ch1;
        int x2=ch2;
        System.out.println(x1+"\t\t"+x2);

        //narrowing
        int x3=67;
        char ch3= (char) x3;
        System.out.println(ch3);

        double d=69.0;
        char ch4= (char) d;
        System.out.println(ch4);
    }
}
