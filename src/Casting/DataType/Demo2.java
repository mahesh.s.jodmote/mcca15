package Casting.DataType;

public class Demo2{
    public static void main(String[] args) {
        int x=53;
        float y=23.5f;

        int a= (int) y; //narrowing
        float b=x;      //widening

        System.out.println(a+"\t\t"+b);
    }
}
