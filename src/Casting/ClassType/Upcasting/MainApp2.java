package Casting.ClassType.Upcasting;

public class MainApp2 {
    public static void main(String[] args) {
        TV t1;
        t1=new LCD();//upcasting
        t1.displayPicture();


        t1=new LED();//upcasting
        t1.displayPicture();
    }
}
