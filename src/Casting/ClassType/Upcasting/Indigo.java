package Casting.ClassType.Upcasting;

public class Indigo extends Goibio{
    double[]cost={3000.25,7000.36,10000.52};

    @Override
    void bookTicket(int qty, int routechoice) {
        boolean found=false;
        for (int a = 0; a <= routes.length; a++) {
            if (routechoice==a) {
                double total = qty * cost[a];
                System.out.println("Total Amount:" + total);
                found = true;

                if (!found) {
                    System.out.println("Invalid Choice");
                }
            }
        }
    }
}
