package Casting.ClassType.Upcasting;
//subclass
public class Laptop extends Machine{
    @Override
    void getType() {
        System.out.println("Machine type is Laptop");
    }

    @Override
    void calculateBill(int qty, double price) {
        double total=qty*price;
        double finalAmt=total+total*0.15;
        System.out.println("Final Amount:"+finalAmt);
    }
}
