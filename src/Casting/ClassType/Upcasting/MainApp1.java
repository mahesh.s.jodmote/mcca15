package Casting.ClassType.Upcasting;

public class MainApp1 {
    public static void main(String[] args) {
        Sample s1=new Sample();
        s1.Info();
        s1.test();

        Demo d1=new Sample();//upcasting
        d1.test();
    }
}
