package Casting.ClassType.Downcasting;

public class MainApp1 {
    public static void main(String[] args) {
        Master m1=new Central();///upcasting
        m1.test();

        Central c1= (Central) new Master();//downcasting
        c1.display();
        c1.test();

    }
}
