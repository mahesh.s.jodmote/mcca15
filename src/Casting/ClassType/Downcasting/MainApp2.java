package Casting.ClassType.Downcasting;

public class MainApp2 {
    public static void main(String[] args) {
        Master m1=new Master();//object of superclass
        if (m1 instanceof Central){
            Central c1=(Central) m1;
        }
        else {
            System.out.println("Properties not found");
        }
    }
}
