package DesignPattern;

public class GoogleAccount {
    //static int count
    static GoogleAccount g1;

    private GoogleAccount(){
        //constructor
    }
    static GoogleAccount login(){
        if (g1==null){
            //singleton object
            g1=new GoogleAccount();
            System.out.println("login successfully");
        }
        else {
            System.out.println("Already logged in");
        }
        return g1;
    }
    void accessGmail(){
        System.out.println("Accessing Gmail...");
    }
    void accessDrive(){
        System.out.println("Accessing Drive...");
    }
}
