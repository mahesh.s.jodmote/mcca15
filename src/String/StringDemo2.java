package String;

public class StringDemo2 {
    public static void main(String[] args) {
        String s1="Software Developer";
        System.out.println(s1.length());
        System.out.println(s1.charAt(9));
        System.out.println(s1.indexOf('w'));
        System.out.println(s1.lastIndexOf('r'));
        System.out.println(s1.contains("eve"));//true
        System.out.println(s1.startsWith("Soft"));//true
        System.out.println(s1.endsWith("per"));//true
        System.out.println(s1.substring(9));
        System.out.println(s1.substring(0,8));
        System.out.println(s1.toLowerCase());
        System.out.println(s1.toUpperCase());
    }
}
