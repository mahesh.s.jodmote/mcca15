package String;

import java.util.Scanner;

public class StringDemo4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter String:");
        String name= sc.next();
        String newvalue=name.toLowerCase();
        char[]data=newvalue.toCharArray();
        int vCount=0;
        int cCount=0;
        for(int a=0;a< data.length;a++){
            if(data[a]=='a'||data[a]=='e'||data[a]=='i'||data[a]=='o'||data[a]=='u'){
                vCount++;
            }
            else {
                cCount++;
            }
        }
        System.out.println("total no of vowels is:"+vCount);
        System.out.println("total no of consanants:"+cCount);
    }
}
