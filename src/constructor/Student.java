package constructor;

public class Student {
    int studentId;
    String studentName;
    double studentPercentage;
    Student(int id,String name,double percent){
        studentId=id;
        studentName=name;
        studentPercentage=percent;
    }
    void display() {
        System.out.println(studentId + "\t" + studentName + "\t" + studentPercentage);
    }
}
class mainApp2{
    public static void main(String[] args) {
        Student s1=new Student(101,"mahesh",86.52);
        s1.display();
    }
}
