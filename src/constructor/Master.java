package constructor;

public class Master {
    static{
        System.out.println("static block of master");
    }
    Master(){
        System.out.println("Constructor");
    }
    {
        System.out.println("non-static block");
    }
    void info(){
        System.out.println("method");
    }
}
class mainApp4{
    public static void main(String[] args) {
        System.out.println("main method");
        Master m1=new Master();
        m1.info();
    }
}
