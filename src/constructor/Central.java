package constructor;

public class Central {
    int k;
    double d;
    Central(int k,double d){
        this.k=k;
        this.d=d;
    }
    void display(){
        System.out.println(this);
        System.out.println("k:"+k);
        System.out.println("d:"+d);
    }
}
class mainApp6{
    public static void main(String[] args) {
        Central c1=new Central(10,56.66);
        c1.display();
        System.out.println(1);
    }
}
