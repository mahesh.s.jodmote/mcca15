package constructor;

import java.util.Scanner;

public class laptop {
    String lcompany;
    int lprice;
    int lramsize;
    laptop(String comany,int price,int ramsize){
        lcompany=comany;
        lprice=price;
        lramsize=ramsize;
    }
    void displayDetails(){
        System.out.println(lcompany+"\t"+lprice+"\t"+lramsize);
    }
}
class mainApp3{
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter laptop company name:");
        String comapny= sc1.next();
        System.out.println("enter laptop price:");
        int price= sc1.nextInt();
        System.out.println("enter laptop ramsize:");
        int ramsize= sc1.nextInt();
        laptop l1=new laptop(comapny,price,ramsize);
        l1.displayDetails();
    }
}
