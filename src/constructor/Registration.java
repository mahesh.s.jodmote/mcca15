package constructor;

public class Registration {
    String username;
    int contact;
    String emilid;
    String address;
    Registration(String name,int contactno,String email){
        username=name;
        contact=contactno;
        emilid=email;
    }
    Registration(String name,int contactno,String email,String currentaddress){
        username=name;
        contact=contactno;
        emilid=email;
        address=currentaddress;
    }
    void displayDetails(){
        System.out.println(username);
        System.out.println(contact);
        System.out.println(emilid);
        System.out.println(address);
    }
}
class mainApp5{
    public static void main(String[] args) {
        Registration r1=new Registration("admin",1234,"maj@gmailcom");
        r1.displayDetails();
        Registration r2=new Registration("maheshjodmote",7532434,"mj@gmailc.om","pune");
        r2.displayDetails();
    }
}
