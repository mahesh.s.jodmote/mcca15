package Inheritance.Hierarchical;

public class MainApp3 {
    public static void main(String[] args) {
        PermanantEmp p1=new PermanantEmp();
        p1.getInfo(101,30000);
        p1.getDesignation("Analyst");

        ContractEmp c1=new ContractEmp();
        c1.getInfo(102,50000);
        c1.getContractDetails(12);
    }
}
