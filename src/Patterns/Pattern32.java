package Patterns;

public class Pattern32 {
    public static void main(String[] args) {
        int line =5;
        int star=4;
        for (int i=0;i<line;i++){
            // int a=1;
            for (int j=0;j<star;j++){
                System.out.print("*"+"\t");
            }
            System.out.println();
            star--;
        }
    }
}
/*

 *	*	*	*
 *	*	*
 *	*
 *

 */
