package Patterns;

public class Pattern41 {
    public static void main(String[] args) {
        char a='A';
        int line = 9;                           //       A
        int star = 1;                           //      B  C
        int space=9;                            //    D  E  F
        for (int i = 0; i <line; i++)           //   G  H  I  J
        {                                       // K  L  M  N  O
            for (int k=0;k<space;k++)           //   P  Q  R  S
            {                                   //    T  U  V
                System.out.print(" ");          //      W  X
            }                                   //       Y
            for (int j=0;j<star;j++){
                System.out.print(a+++" ");
            }
            System.out.println();
            if(i<=3)
            {
                space--;
                star++;
            }
            else
            {
                star--;
                space++;
            }
        }
    }
}
