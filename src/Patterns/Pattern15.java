package Patterns;

public class Pattern15 {
    public static void main(String[] args) {
        int line=5;


        for (int i=0;i<line;i++)
        {                                                   //1 2 3 4 5
            int ch=1;                                       //1 2 3 4 5
            for (int  j=0;j<5;j++)                          //1 2 3 4 5
            {
                System.out.print(ch+"\t");
                ch++;
            }
            System.out.println();

        }
    }
}
