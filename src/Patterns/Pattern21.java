package Patterns;

public class Pattern21 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
       // int ch=1;
        for (int i=0;i<line;i++)
        {
            int ch=1;                                          //1 * 2 * 3
            for (int j=0; j<star;j++)                          //1 * 2 * 3
            {                                                  //1 * 2 * 3
                if ((j==0)||(j==2)||(j==4))                    //1 * 2 * 3
                {                                              //1 * 2 * 3
                    System.out.print(ch+++"\t");
                }
                else
                {
                    System.out.print("*"+"\t");
                }
            }
            System.out.println();
        }
    }
}
