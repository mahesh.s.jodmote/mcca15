package Patterns;

public class Pattern23 {
    public static void main(String[] args) {
        int line=5;
        int star=4;
        for (int i=0;i<line;i++)
        {   int a=1;
            char b='A';                             //1 2 3 4
            for (int j=0; j<star; j++)              //A B C D
            {                                       //1 2 3 4
                if ((i==0)||(i==2)||(i==4))         //A B C D
                {                                   //1 2 3 4
                    System.out.print(a+++"\t");
                }
                else
                {
                    System.out.print(b+++"\t");
                }
            }
            System.out.println();
        }
    }
}
