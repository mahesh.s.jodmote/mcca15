package Patterns;

public class Pattern13 {
    public static void main(String[] args) {
        int line=5;
        int ch=1;

        for (int i=0;i<line;i++)                            //1 1 1 1 1
        {                                                   //2 2 2 2 2
            for (int  j=0;j<5;j++)                          //3 3 3 3 3
            {                                               //4 4 4 4 4
                System.out.print(ch+"\t");                  //5 5 5 5 5
            }
            System.out.println();
            ch++;
        }
    }
}
