package Patterns;

public class Pattern22 {
    public static void main(String[] args) {
        int line=5;
        int star=6;
        for (int i=0;i<line;i++)
        {
            int a=1;                                //1 * 3 * 5 *
            for (int j=0; j<star; j++)              //1 * 3 * 5 *
            {                                       //1 * 3 * 5 *
                if ((j==0)||(j==2)||(j==4))         //1 * 3 * 5 *
                {                                   //1 * 3 * 5 *
                    System.out.print(a+"\t");

                }
                else
                {
                    System.out.print("*"+"\t");
                }
                a++;
            }
            System.out.println();
        }
    }
}
