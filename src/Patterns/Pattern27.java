package Patterns;

public interface Pattern27 {
    public static void main(String[] args) {
        int line = 5;                           //                  1
        int star = 1;                           //              1	2
        int space=line-1;                       //          1	2	3
        for (int i = 0; i<line; i++)           //       1	2	3	4
        {     int a=1;                         //    1	2	3	4	5
            for (int k=0;k<space;k++)
            {
                System.out.print(" "+"\t");
            }
            for (int j=0;j<star;j++){
                System.out.print(a+++"\t");
            }
            space--;
            star++;
            System.out.println();
        }
    }
}
