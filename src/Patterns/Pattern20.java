package Patterns;

public class Pattern20 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int ch=1;
        for (int i=0;i<line;i++)
        {                                           //1 1 1 1 1
            for (int j=0; j<star; j++)              //* * * * *
            {                                       //3 3 3 3 3
                if ((i==0)||(i==2)||(i==4))         //* * * * *
                {                                   //5 5 5 5 5
                    System.out.print(ch+"\t");
                }
                else{
                    System.out.print("*"+"\t");
                }
            }
            ch++;
            System.out.println();
        }
    }
}
