package Patterns;

public class Pattern31 {
    public static void main(String[] args) {
        int line =5;
        int star=1;
        for (int i=0;i<line;i++){
             int a=1;
            for (int j=0;j<star;j++){
                System.out.print(a+++"\t");
            }
            System.out.println();
            star++;
        }
    }
}
/*

1
1	2
1	2	3
1	2	3	4
1	2	3	4	5
 */
