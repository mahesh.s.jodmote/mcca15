package Patterns;

public class Pattern19 {
    public static void main(String[] args) {
        int line=4;                                 //A B C D
        char ch='A';                                //E A B C
        for (int i=0;i<line;i++)                    //D E A B
        {                                           //C D E A
            for (int j=0; j<4; j++)
            {
                System.out.print(ch++ + "\t");
                if (ch >'E')
                {
                    ch='A';
                }
            }
            System.out.println();
        }
    }
}
