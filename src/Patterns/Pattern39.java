package Patterns;

public class Pattern39 {
    public static void main(String[] args) {
        int a=1;
        int line = 4;                           //    1
        int star = 1;                           //   2 3
        int space=4;                            //  4 5 6
        for (int i = 0; i <line; i++)           // 7 8 9 10
        {
            for (int k=0;k<space;k++)
            {
                System.out.print(" ");
            }
            for (int j=0;j<star;j++){
                System.out.print(a+++" ");
            }
            System.out.println();
            space--;
            star++;
        }
    }
}
