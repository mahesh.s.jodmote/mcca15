package Patterns;

public class Pattern25 {
    public static void main(String[] args) {       //M              M
        int line=5;                                //M  M       M   M
        int star=5;                                //M      M       M
        for (int i=0;i<line;i++)                   //M              M
        {                                          //M              M
            for (int j=0; j<star; j++)
            {
                if ((j==0)||(j==4)||i==j&&!(i+j==6)||(i<2)&&(j<=3)&&(i+j==4))
                {
                    System.out.print("M"+"\t");

                }
                else
                {
                    System.out.print(" "+"\t");
                }
            }
            System.out.println();
        }
    }

}
