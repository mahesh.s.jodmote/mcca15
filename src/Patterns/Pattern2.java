package Patterns;

public class Pattern2 {
    public static void main(String[] args) {
        int a=5; int b=5;                                                             //* * * * *
        for (int i=0;i<a;i++)                                                         //*       *
        {                                                                             //*       *
            for (int j=0;j<b;j++)                                                     //*       *
            {                                                                         //* * * * *
                if(j==0||j==4||i==4||i==0)
                {
                    System.out.print("*"+"\t");
                }
                else
                {
                    System.out.print(" "+"\t");
                }

            }
            System.out.println();
        }
    }
}
