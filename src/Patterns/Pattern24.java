package Patterns;

public class Pattern24 {
    public static void main(String[] args) {                                
        int line=5;
        int star=2;
        for (int i=0;i<line;i++)
        {
                if (i==0){ for (int j=0; j<star; j++){System.out.print(i+1+"\t");}}
                if (i==1){ for (int j=0; j<star; j++){System.out.print("*"+"\t");}}
                if (i==2){ for (int j=0; j<star; j++){System.out.print(i+"\t");}}
                if (i==3){ for (int j=0; j<star; j++){System.out.print("*"+"\t");}}
                if (i==4){ for (int j=0; j<star; j++){System.out.print(i-1+"\t");}}
                star++;
                System.out.println();
        }
    }
}
