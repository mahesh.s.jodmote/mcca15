package Patterns;

public class Pattern35 {
    public static void main(String[] args) {
        int line = 5;                           //   *  *   *   *   *
        int star = 4;                            //      *   *   *	*
        int space=1;                            //           *   *	*
        for (int i = 0; i <line;i++)            //          	*	*
        {                                       //              	*
            for (int k=0;k<space;k++)
            {
                System.out.print(" "+"\t");
            }
            for (int j=0;j<star;j++){
                System.out.print("*"+"\t");
            }
            System.out.println();
            space++;
            star--;
        }
    }
}
