package Patterns;

public class Pattern16 {
    public static void main(String[] args) {
        int line=5;

        int ch1=1;
        for (int i=0;i<line;i++)
        {                                                   //1 2 3 4 5
              int ch2=ch1;                                  //2 3 4 5 6
            for (int  j=0;j<5;j++)                          //3 4 5 6 7
            {                                               //4 5 6 7 8
                System.out.print(ch2++ +"\t");              //5 6 7 8 9

            }
            System.out.println();
            ch1++;

        }
    }
}
