package Patterns;

public class Pattern17 {
    public static void main(String[] args) {
        int line=4;
        int ch1=1;
        for (int i=0;i<line;i++)
        {                                                   //1 2 3 4
            int ch2=1;                                      //2 4 6 8
            for (int  j=0;j<4;j++)                          //3 6 9 12
            {                                               //4 8 12 16
                System.out.print(ch2++ * ch1 +"\t");
            }
            System.out.println();
            ch1++;
        }
    }
}
