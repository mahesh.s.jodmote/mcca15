package Patterns;

public class Pattern28 {
    public static void main(String[] args) {
        int line= 5;            //	 	   5
        int star=1;             //       4 5
        int space=line-1;       //   2 3 4 5
        int a=5;                // 1 2 3 4 5
        for (int i=0;i<line;i++)
        {   int b=a;
            for (int j=0;j<space;j++)
            {
                System.out.print(" "+"\t");
            }
            for (int j=0;j<star;j++)
            {
                System.out.print(b+++"\t");
            }
            System.out.println();
            a--;
            star++;
            space--;
        }
    }
}
