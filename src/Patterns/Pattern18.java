package Patterns;
//important mindtech
public class Pattern18 {
    public static void main(String[] args) {
        int line=4;                                 //1 2 3 4 5
        int ch=1;                                   //6 7 1 2 3
        for (int i=0;i<line;i++)                    //4 5 6 7 1
        {                                           //2 3 4 5 6
             for (int j = 0; j < 5; j++)
             {
                 System.out.print(ch++ + "\t");
                if (ch >7)
                {
                    ch=1;
                }
            }
            System.out.println();
        }

    }
}


