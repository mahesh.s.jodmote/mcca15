package Patterns;

public class Pattern44 {
    public static void main(String[] args) {
        int a=5;
        int b=5;
        for (int row=0;row<=a;row++)
        {
            for (int col=0;col<=b;col++)
            {
                if(col==1||col==3){
                    System.out.print("0");
                }else {
                    System.out.print("1");
                }
            }
            System.out.println();
        }
    }
}
