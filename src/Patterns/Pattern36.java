package Patterns;

public class Pattern36 {
    public static void main(String[] args) {
        int line = 9;                           //                  *
        int star = 1;                           //              *	*
        int space=4;                            //          *	*	*
        for (int i = 0; i <line; i++)           //      *	*	*	*
        {                                       //   *	*	*	*	*
            for (int k=0;k<space;k++)           //      *   *   *   *
            {                                   //          *   *   *
                System.out.print(" "+"\t");     //              *   *
            }                                   //                  *
            for (int j=0;j<star;j++){
                System.out.print("*"+"\t");
            }
            System.out.println();
            if(i<=3)
            {
                space--;
                star++;
            }
            else
            {
                star--;
                space++;
            }
        }
    }
}
