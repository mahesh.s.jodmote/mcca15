package Patterns;

public class Pattern14 {
    public static void main(String[] args) {
        int line=5;
        int ch=1;

        for (int i=0;i<line;i++)                            //1 2 3 4 5
        {                                                   //6 7 8 9 10
            for (int  j=0;j<5;j++)                          //11 12 13 14 15
            {
                System.out.print(ch+"\t");
                ch++;
            }
            System.out.println();

        }
    }
}
