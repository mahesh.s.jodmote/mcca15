package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo3 {
    public static void main(String[] args) {
        //generic array list
        ArrayList<String >data=new ArrayList<>();

        data.add("JAVA");
        data.add("SQL");
        data.add("J2EE");
        data.add("Manual");

        //Option-1
        System.out.println(data);
        System.out.println("============================");
        //Option-2
        for (int i = 0; i < data.size(); i++)
        {
            System.out.println(data.get(i));
        }
        System.out.println("==============================");
        //Option-3
        for (String s:data)
        {
            System.out.print(s+" ");
        }
    }
}
