package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo2 {
    public static void main(String[] args) {
        //generic arraylist same type data
        ArrayList<Integer>data=new ArrayList<>();
        data.add(25);
        data.add(30);
        data.add(35);
        data.add(40);
        System.out.println(data);
    }
}
