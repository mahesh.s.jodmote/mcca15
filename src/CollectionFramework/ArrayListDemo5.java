package CollectionFramework;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import java.util.ArrayList;
import java.util.ListIterator;

public class ArrayListDemo5 {
    public static void main(String[] args) {
        ArrayList<Character>data=new ArrayList<>();
        data.add('T');
        data.add('R');
        data.add('S');

        //Option-5:ListIterator Interface
        ListIterator<Character>itr=data.listIterator();
        while (itr.hasNext()){
            System.out.println(itr.next());
        }
        System.out.println("==========");
        while (itr.hasPrevious()){
            System.out.println(itr.previous());
        }
    }
}
