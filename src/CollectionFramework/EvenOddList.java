package CollectionFramework;

import java.util.ArrayList;
import java.util.Scanner;

public class EvenOddList {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter start point");
        int start= sc.nextInt();
        System.out.println("Enter end point");
        int end=sc.nextInt();
        ArrayList<Integer>even=new ArrayList<>();
        ArrayList<Integer>odd=new ArrayList<>();
        for (int a=start;a<=end;a++){
            if(a%2==0){
                even.add(a);
            }
            else {
                odd.add(a);
            }
        }
        System.out.println("Even number:"+even);
        System.out.println("Odd number:"+odd);
    }
}
