package CollectionFramework;

import java.util.ArrayList;

public class ArrayListDemo1 {
    public static void main(String[] args) {
        //non-generic arry list

        ArrayList data=new ArrayList();
        data.add(35);
        data.add(38.25);
        data.add("Java");
        data.add('A');
        data.add(true);
        data.add(35);
        data.add(null);
        System.out.println(data);
    }
}
