package SeriesProgramming;
//armstrong means the numbers powers sum is equal to that number
public class SpArmstrongnum {
    public static void main(String[] args) {
        int a=153;
        int sum=0;
        while(a!=0){
            int r=a%10;
            a=a/10;
            sum=sum+(r*r*r);
        }
        if (sum==153){
            System.out.println("number is armstrong");
        }
        else{
            System.out.println("number is not armstrong");
        }
    }
}
