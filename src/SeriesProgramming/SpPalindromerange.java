package SeriesProgramming;
//palindrome in the given range
public class SpPalindromerange {
    public static void main(String[] args) {
        for (int i=10;i<=1000;i++)
        {
            int a=i;
            int sum=0;
            int temp=a;
            while (a != 0)
            {
                int r = a % 10;
                sum = (sum * 10) + r;
                a = a / 10;
            }
            if (sum==temp)
            {
                System.out.println(sum);
            }
        }
    }
}
