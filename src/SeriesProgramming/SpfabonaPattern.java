package SeriesProgramming;
//finonacci series with pattern
public class SpfabonaPattern {
    public static void main(String[] args) {  //0
        int line=5;                           //0 1 1
        int star=1;                           //0 1 1 2 3
        for (int i=1;i<=line;i++)             //0 1 1 2 3 5 8
        {                                     //0 1 1 2 3 5 8 13 21
            int n1=0,n2=1;
            for (int j=0;j<star;j++)
            {
                System.out.print(n1+" ");
                int a=n1+n2;
                n1=n2;
                n2=a;
            }
            System.out.println();
            star+=2;
        }
    }
}
