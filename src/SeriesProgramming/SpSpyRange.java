package SeriesProgramming;

public class SpSpyRange {
    public static void main(String[] args) {
        for (int i = 10; i <=10000 ; i++) {
            int a=i;
            int sum=0,mul=1;
            while(a!=0){
                int b=a%10;
                sum=sum+b;
                mul=mul*b;
                a=a/10;
            }
            if (sum==mul){
                System.out.println(i);
            }
        }
    }
}
