package SeriesProgramming;

public class SpFactorialRange {
    public static void main(String[] args) {
        for (int j = 1; j <=5; j++)
        {
            int n=j;
            long fact=1;
            for(int i=1;i<=n;i++){
                fact=fact*i;
            }
            System.out.println(fact);
        }
    }
}
