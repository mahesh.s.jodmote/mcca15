package SeriesProgramming;
//reverse the number
public class SpDemo4 {
    public static void main(String[] args) {
        int a=121;
        int sum=0;
        while(a!=0)
        {
            int r=a%10;
            System.out.print(r);
            sum=(sum*10)+r;
            a=a/10;
        }
        System.out.println(sum);        //43214321
    }
}
