package CollectionProgramming;

import java.util.HashSet;
import java.util.Set;

public class Hashset1 {
    public static void main(String[] args) {
        Set<Character>hs1=new HashSet<>();
        String str="CORE JAVA PROGRAMMING";
        for (int i = 0; i < str.length(); i++) {
            hs1.add(str.charAt(i));
        }
        for (Character c:hs1){
            System.out.print(c+" ");
        }
    }
}
