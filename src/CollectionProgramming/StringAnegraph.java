package CollectionProgramming;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class StringAnegraph {
    public static void main(String[] args) {
        Set<Character>hs1=new HashSet<>();
        String str1="army";
        String str2="mary";
        int n1=str1.length();
        int n2=str2.length();
        if (n1==n2)
        {
            char[]arr1=str1.toCharArray();
            char[]arr2=str2.toCharArray();

            Arrays.sort(arr1);
            Arrays.sort(arr2);
            boolean status=true;
            for (int i = 0; i < n1; i++) {
                if (arr1[i]!=arr2[i]){
                    status=false;
                }
            }
            if (status){
                System.out.println("string is anagram");
            }
            else {
                System.out.println("String is not anagram");
            }
        }
    }
}
