package Abstraction;

public class SavingAccount implements Account{
    double savingAmount;
    public SavingAccount(double savingAmount) {
        this.savingAmount=savingAmount;
        System.out.println("SAVING ACCOUNT CREATED");
    }

    @Override
    public void deposite(double amt) {
        savingAmount+=amt;
        System.out.println(amt+"Rs credited to your account");
    }
    @Override
    public void withdraw(double amt) {
        if (amt<savingAmount) {
            savingAmount -= amt;
            System.out.println(amt + "Rs Debited to your account");
        }
        else{
            System.out.println("Insufficient Balance");
        }
    }
    @Override
    public void checkBalance() {
        System.out.println("Active Balance "+savingAmount);
    }
}
