package Abstraction;

public class CFLLight implements switchDemo{
    @Override
    public void switchOn() {
        System.out.println("CFL LIGHT SWITCH ON");
    }

    @Override
    public void switchOff() {
        System.out.println("CFL LIGHT SWITCH OFF");
    }
}
