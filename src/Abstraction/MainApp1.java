package Abstraction;
//utilization layer
public class MainApp1 {
    public static void main(String[] args) {
        switchDemo s;
        s=new LedLight();//upcasting
        s.switchOn();
        s.switchOff();

        s=new CFLLight();//upcasting
        s.switchOn();
        s.switchOff();
    }
}
