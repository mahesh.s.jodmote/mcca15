package Abstraction;

public class LedLight implements switchDemo{
    @Override
    public void switchOn() {
        System.out.println("LED LIGHT SWITCH ON");
    }

    @Override
    public void switchOff() {
        System.out.println("LED LIGHT SWITCH OFF");
    }
}
