package Abstraction;

public class LoanAccount implements Account{
    double loanAmount;
    public LoanAccount(double loanAmount) {
        this.loanAmount=loanAmount;
        System.out.println("LOAN ACCOUNT CREATED");
    }
    @Override
    public void deposite(double amt) {
        loanAmount-=amt;
        System.out.println(amt+"Rs Debited to your account");
    }
    @Override
    public void withdraw(double amt) {
    loanAmount+=amt;
        System.out.println(amt+"Rs Debited to your account");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active balance "+loanAmount);
    }
}
