package Composition;

public class Laptop {
    static HDD h1=new HDD();//static member
    HDD h2=new HDD();//non-static member
    void getInfo(){
        System.out.println("Company name is Dell");
        System.out.println("Price is 45000");
    }
}
