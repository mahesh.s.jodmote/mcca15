package Composition;
//Dependency entity
public class Salesman {
    Product p1=new Product();
    void ProvideService(){
        p1.getInfo();
        System.out.println("Service is Provided");
    }
}
