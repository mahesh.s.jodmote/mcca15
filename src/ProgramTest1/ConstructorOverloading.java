package ProgramTest1;

import org.omg.PortableServer.IdAssignmentPolicy;

import java.security.SecureRandom;

public class ConstructorOverloading {
            int eId;
            String eName;
            double eSalary;
            String eAddress;
            ConstructorOverloading(int Id,String Name,double Salary){
                eId=Id;
                eName=Name;
                eSalary=Salary;
            }
            ConstructorOverloading(int Id, String Name, double Salary,String Address){
                eId=Id;
                eName=Name;
                eSalary=Salary;
                eAddress=Address;
            }
            void Display(){
                System.out.println("empId:"+eId+"\t"+"empName:"+eName+"\t"+"empSalary:"+eSalary+"\t"+"empAddrss:"+eAddress);
            }
}
