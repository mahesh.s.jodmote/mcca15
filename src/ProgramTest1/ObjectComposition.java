package ProgramTest1;

public class ObjectComposition {
    //state
    String company="Samsung";
    double price=35000.21;
    boolean isNFCenabled=true;
    int ramsize=4;

    //behaviour
    void calling(){
        System.out.println("make and receive call");
    }
    void installapp(){
        System.out.println("installing apps");
    }
}
