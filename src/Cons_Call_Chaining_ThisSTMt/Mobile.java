package Cons_Call_Chaining_ThisSTMt;

public class Mobile extends Product{
    double productPrice=50000;
    int qty=2;

    void info(){
        System.out.println("Price is:"+productPrice);
        System.out.println("Quantity is:"+qty);
        System.out.println("Price is:"+super.productPrice);
        System.out.println("Quantity is:"+super.qty);
    }
}
