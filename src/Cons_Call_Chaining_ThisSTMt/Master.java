package Cons_Call_Chaining_ThisSTMt;

public class Master {
     public Master(int a)
    {

        System.out.println(a);
    }

    public Master(String s) {
         this(25);
        System.out.println(s);
    }
    public Master(char c) {
         this("JAVA");
        System.out.println(c);
    }

}
