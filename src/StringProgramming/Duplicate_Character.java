package StringProgramming;

import java.util.HashSet;
import java.util.Set;
//wap to calculate occurrences in the string
public class Duplicate_Character {
    public static void main(String[] args) {
        Set<Character> hs1=new HashSet<>();
        String str1="Core Java Programming";
        for (int i = 0; i < str1.length(); i++) {
            char ch=str1.charAt(i);
            hs1.add(ch);
        }
        for (Character c:hs1){
            int count=0;
            for (int i = 0; i < str1.length(); i++) {
                if (c==str1.charAt(i)){
                    count++;
                }
            }
            System.out.println(c+" Occurances "+count);
        }
    }
}
