package GrommingClass;

import java.util.Scanner;

public class MainApp {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        HotelMenu h1=new HotelMenu();
        boolean status=true;
        while (status){
            System.out.println("Select Mode of Activity");
            System.out.println("1:DisplayMenu\n2:ViewBill\n3:Cancel Order\n4:exit");
            int choice= sc.nextInt();
            if(choice==1){
                h1.displaymenu();
            } else if (choice==2) {
                h1.viewbill();
            } else if (choice==3) {
                h1.cancelorde();
            }
            else {
                status=false;
            }
        }

    }
}
