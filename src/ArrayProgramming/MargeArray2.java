package ArrayProgramming;

import java.util.Arrays;

public class MargeArray2 {
    public static void main(String[] args) {


        int[]arr1={1,2,3,4};
        int[]arr2={5,6,7,8};//output
        int a= arr1.length; //[1,5,2,6,3,7,4,8]
        int b= arr2.length;
        int[] arr3=new int [a+b];
        int idx=0;
        for (int c:arr1)
        {
            arr3[idx]=c;
            idx+=2;
        }
        idx=1;
        for (int c:arr2)
        {
            arr3[idx]=c;
            idx+=2;
        }
        System.out.println(Arrays.toString(arr3));
    }
}
