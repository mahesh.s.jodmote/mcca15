package ArrayProgramming;

public class ReverseArray {                //output:
    public static void main(String[]args){//1 2 3 4 5 6
        int[]arr={1,2,3,4,5,6};           //6 5 4 3 2 1
        for (int a:arr)
        {
            System.out.print(a+" ");
        }
        int count= arr.length-1;
        for (int i = 0; i < arr.length/2 ; i++)
        {
            int temp=arr[i];
            arr[i]=arr[count];
            arr[count]=temp;
            count--;
        }
        System.out.println();
        for (int a:arr)
        {
            System.out.print(a+" ");
        }
    }
}
