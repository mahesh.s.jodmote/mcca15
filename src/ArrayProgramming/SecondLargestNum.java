package ArrayProgramming;

public class SecondLargestNum {
    public static void main(String[] args) {
        int[] arr = {2,45,12,6,8,15,11,4, 2, 1, 10, 45, 5,};
        int max1 = arr[0];
        int max2 = arr[1];
        for(int i=0;i<arr.length;i++){
         if(arr[i]>max1){
             max2=max1;
                max1=arr[i];
            } else if (arr[i]>max2 && arr[i]!=max1) {
             max2=arr[i];
         }
        }
        System.out.println("Second Largest number is:" + max2);
        System.out.println("Largest number is:"+max1);
    }
}
