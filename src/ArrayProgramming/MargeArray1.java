package ArrayProgramming;

import java.util.Arrays;

public class MargeArray1 {
    public static void main(String[] args) {
        int[]arr1={1,2,3,4};
        int[]arr2={5,6,7,8,9};//output
        int a= arr1.length;   //[1,2,3,4,5,6,7,8,9]
        int b= arr2.length;
        int[] arr3=new int [a+b];
        int idx=0;
        for (int c:arr1)
        {
            arr3[idx]=c;
            idx++;
        }
        for (int c:arr2)
        {
            arr3[idx]=c;
            idx++;
        }
        System.out.println(Arrays.toString(arr3));
    }
}
