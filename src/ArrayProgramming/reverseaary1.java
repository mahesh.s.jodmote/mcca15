package ArrayProgramming;

public class reverseaary1 {
    public static void main(String[] args) {
        int[]arr={1,2,3,4,5,6};             //output:
        for (int a:arr)                     //1 2 3 4 5 6
        {                                   //4 5 6 1 2 3
            System.out.print(a+" ");
        }
        int count= arr.length/2;
        for (int i = 0; i < arr.length/2 ; i++)
        {
            int temp=arr[i];
            arr[i]=arr[count];
            arr[count]=temp;
            count++;
        }
        System.out.println();
        for (int a:arr)
        {
            System.out.print(a+" ");
        }
    }
}
