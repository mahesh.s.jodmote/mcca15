package ArrayProgramming;

public class EnhancePattern {
    public static void main(String[] args) {
        char[]arr={'A','B','C','D'};            //S T A R  //A B C D
        int star=4;                             //T A R S  //B C D A
        int line=4;                             //A R S T  //C D A B
                                                //R S T A  //D A B C
        for (int i = 0; i < line; i++)
        {
            int a=i;
            for (int j = 0; j < star; j++)
            {
                System.out.print(arr[a]+" ");
                a++;
                if (a>3)
                {
                    a=0;
                }
            }
            System.out.println();
        }
    }
}
