package ArrayProgramming;

public class LargestNumfind {
    public static void main(String[] args) {
        int []arr={1,2,10,4,5,6};
        int max=arr[0];
        for (int a:arr){   //for(i=0;i<arr.length;i++)
            if (a>max){    //if(arr[i]>max)
                max=a;     //max=arr[i]
            }
        }
        System.out.println("Largest number is:"+max);
    }
}
