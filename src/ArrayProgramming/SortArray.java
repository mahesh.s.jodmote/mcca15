package ArrayProgramming;

import java.util.Arrays;
//wap to bubble sort
public class SortArray {
    public static void main(String[] args) {
       // Arrays.sort();
        int[]arr={5,3,6,2,1};
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr.length-1; j++)
            {
                if (arr[j]>arr[j+1])
                {
                     int temp=arr[j];
                     arr[j]=arr[j+1];
                     arr[j+1]=temp;
                     j--;
                }
            }
        }
        for (int a:arr){
            System.out.print(a+" ");
        }
    }
}
